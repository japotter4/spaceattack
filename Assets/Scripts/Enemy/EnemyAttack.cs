﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EnemyAttack : MonoBehaviour {

    public GameObject bulletPrefab;
    private GameObject PlayerShip;
    public float reloadRate; // In seconds;
    public float fireRange;

    private float _lastShot = 0;

    void Awake() {
        PlayerShip = GameObject.Find("Player");
    }

    void Update () {
        transform.right = PlayerShip.transform.position - transform.position;

        if (Time.time - _lastShot > reloadRate) {
            if (Vector2.Distance(transform.position, PlayerShip.transform.position) < fireRange) {
                // Fire
                GameObject bullet = GameObject.Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
                bullet.transform.right = PlayerShip.transform.position - transform.position;

                _lastShot = Time.time;
            }
        }
    }
}
