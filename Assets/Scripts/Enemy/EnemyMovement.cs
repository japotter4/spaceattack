﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EnemyMovement : MonoBehaviour {

    public float maxDistance; // Distance ship should be from player
    public float minDistance;
    public float turnAroundDistance;
    public float respawnDistance;
    private GameObject playerShip;
    private Rigidbody2D rigidBody;
    public float speed;

    void Start () {
        playerShip = GameObject.Find("Player");
        rigidBody = transform.GetComponent<Rigidbody2D>();
    }

    void Update() {
        float distance = Vector2.Distance(playerShip.transform.position, transform.position);

        if (distance > respawnDistance) {
            transform.position = EnemySpawner.Instance.GetPositionNearPlayer();
        }

        if (distance > turnAroundDistance) {
            rigidBody.velocity = transform.right.normalized * -speed;
        } else if (distance > maxDistance) {
            rigidBody.velocity = transform.right.normalized * speed;
        } else if (distance < minDistance) {
            rigidBody.velocity = transform.right.normalized * -speed;
        } else {
            rigidBody.velocity = Vector2.zero;
        }
    }
}
