﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnemySpawner : MonoBehaviour {

    private GameObject player;
    public List<GameObject> enemyPrefabs;
    public float distanceFromPlayer;
    public static EnemySpawner Instance;

    public float minTimeBetweenSpawn;
    public float maxTimeBetweenSpawn;
    private List<int> waves = new List<int> { 2, 5, 12, 18, 30, 40 };
    public bool spawning = false;

    void Awake() {
        Instance = this;
    }

    void Start() {
        player = GameObject.Find("Player");
    }

    public void SpawnWave(int wave) {
        StartCoroutine(StartSpawnWave(wave));
    }

    // Accepts wave = [1, ...]
    public IEnumerator StartSpawnWave(int wave) {
        wave -= 1;
        spawning = true;

        if (wave < waves.Count()) {
            int numSpawns = waves[wave];
            int numSpawned = 0;

            while(numSpawned < numSpawns) {
                int enemyType = 1;

                Instantiate(enemyPrefabs[enemyType - 1], GetPositionNearPlayer(), Quaternion.identity);

                numSpawned++;

                if (numSpawned == numSpawns) {
                    spawning = false;
                } else {
                    yield return new WaitForSeconds(Random.Range(minTimeBetweenSpawn, maxTimeBetweenSpawn));
                }
            }
        }
    }

    public void DestroyAllEnemies() {
        StopAllCoroutines();
        FindObjectsOfType<EnemyMovement>().ToList().ForEach(em => Destroy(em.gameObject));
        FindObjectsOfType<BulletMovement>().ToList().ForEach(bm => Destroy(bm.gameObject));
    }

    public Vector2 GetPositionNearPlayer() {
        Vector2 direction = Random.insideUnitCircle.normalized * distanceFromPlayer;
        return (Vector2)player.transform.position + direction;
    }
}
