﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EnemyHealth : MonoBehaviour {

    public int health;
    public int damagePerBullet;

    void OnTriggerEnter2D(Collider2D collider) {
        health -= damagePerBullet;

        if (health <= 0) {
            FindObjectOfType<Game>().EnemyDied();
            Destroy(gameObject);
        }
    }
}
