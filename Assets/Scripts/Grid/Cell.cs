﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public enum CellItem {
    None,
    Turret,
    Thruster,
    Shield,
    Repair,
    Luggage,
    Power,
    Chest,
    Supplies,
}

public class Cell {

    public int startingHealth;
    public int currentHealth;
    private SpriteRenderer spriteRenderer;

    public int row;
    public int col;

    private GameObject baseCell;
    private GameObject disabledSign;
    public CellItem currentItem = CellItem.None;
    public GameObject cellItemObject;
    private PlayerHealth playerHealth;

    public Cell(int row, int col) {
        GameObject baseCellPrefab = Resources.Load("Cell") as GameObject;
        startingHealth = Grid.Instance.cellSprites.Count() - 1;
        Transform player = GameObject.Find("Player").transform;
        playerHealth = GameObject.FindObjectOfType<PlayerHealth>();

        this.row = row;
        this.col = col;

        baseCell = GameObject.Instantiate(baseCellPrefab, Grid.CellPositionToPlayer(this), Quaternion.identity) as GameObject;
        baseCell.transform.SetParent(player);
        disabledSign = baseCell.transform.Find("Disabled").gameObject;
        disabledSign.SetActive(false);
        spriteRenderer = baseCell.GetComponentInChildren<SpriteRenderer>();

        currentHealth = startingHealth;
        UpdateSprites();
    }

    public void ResetItem() {
        GameObject.Destroy(cellItemObject);
        currentItem = CellItem.None;
    }

    public void SetCellItem(CellItem cellItem) {
        currentItem = cellItem;
    }

    public void DisplayCellItem() {
        GameObject.Destroy(cellItemObject);
        cellItemObject = GameObject.Instantiate(Grid.CellItemPrefabs[currentItem], Vector2.zero, Quaternion.identity) as GameObject;

        cellItemObject.transform.SetParent(baseCell.transform, false);

        cellItemObject.BroadcastMessage("ItemPlaced", this, SendMessageOptions.DontRequireReceiver);

        UpdateSprites();
    }

    // Health

    public void FullyHeal() {
        ChangeHealth(startingHealth);
    }

    public void Heal() {
        ChangeHealth(1);
    }

    public void Damage() {
        ChangeHealth(-1);
    }

    private void ChangeHealth(int delta) {
        currentHealth = Mathf.Clamp(currentHealth + delta, 0, startingHealth);
        playerHealth.HealthChanged();
        UpdateSprites();
    }

    private void UpdateSprites() {
        // Show sprite depending on damage;
        spriteRenderer.sprite = Grid.Instance.cellSprites[startingHealth - currentHealth];

        if (currentHealth == 0 && (currentItem != CellItem.None && currentItem != CellItem.Repair)) {
            // Display disabled sign
            disabledSign.SetActive(true);
        } else {
            // Hide disabled sign
            disabledSign.SetActive(false);
        }
    }
}
