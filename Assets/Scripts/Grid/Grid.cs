﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class Grid : MonoBehaviour {

    public List<Sprite> cellSprites;
    public GameObject selectionPrefab;
    public GameObject cellSelectionValidPrefab;
    public GameObject cellSpritePrefab;
    public List<List<Cell>> cells = new List<List<Cell>>();
    public Vector2 currentSelectionPosition = new Vector2(3, 3);
    private Vector2? selectedPosition;
    private GameObject player;
    private GameObject selectionObject;
    private GameObject itemTooltip;

    public static Grid Instance;

    // Previous state
    public List<List<CellItem>> previousStateCells = new List<List<CellItem>>();
    public Vector2 previousStateCurrentSelectionPosition;

    // Static variables

    public static int length = 5;

    public static Dictionary<CellItem, string> CellItemPrefabPaths = new Dictionary<CellItem, string> {
        { CellItem.None, "None" },
        { CellItem.Turret, "Turret" },
        { CellItem.Thruster, "Thruster" },
        { CellItem.Shield, "Shield" },
        { CellItem.Repair, "Repair" },
        { CellItem.Luggage, "Luggage" },
        { CellItem.Power, "Power" },
        { CellItem.Chest, "Chest" },
        { CellItem.Supplies, "Supplies" },
    };
    public static Dictionary<CellItem, GameObject> CellItemPrefabs = new Dictionary<CellItem, GameObject>();

    public static List<List<int>> validPositionInGrid =
        new List<List<int>> {
            new List<int> {0, 0, 1, 1, 0, 0},
            new List<int> {0, 1, 1, 1, 1, 0},
            new List<int> {1, 1, 1, 1, 1, 1},
            new List<int> {1, 1, 1, 1, 1, 1},
            new List<int> {0, 1, 1, 1, 1, 0},
            new List<int> {0, 0, 1, 1, 0, 0},
        };

    public static List<List<int>> validPerimeterInGrid =
        new List<List<int>> {
            new List<int> {0, 0, 1, 1, 0, 0},
            new List<int> {0, 1, 0, 0, 1, 0},
            new List<int> {1, 0, 0, 0, 0, 1},
            new List<int> {1, 0, 0, 0, 0, 1},
            new List<int> {0, 1, 0, 0, 1, 0},
            new List<int> {0, 0, 1, 1, 0, 0},
        };

    void Awake() {
        Instance = this;
        player = GameObject.Find("Player");
        itemTooltip = GameObject.Find("ItemTooltip");
        itemTooltip.SetActive(false);

        //Debug.Log(itemTooltip);

        foreach (KeyValuePair<CellItem, string> path in CellItemPrefabPaths) {
            CellItemPrefabs.Add(path.Key, Resources.Load(path.Value) as GameObject);
        }

        selectionObject = Instantiate(selectionPrefab) as GameObject;
        selectionObject.transform.SetParent(player.transform);
        UpdateSelectionObject();

        // Create board
        for(int row = 0; row <= length; row++) {
            cells.Add(new List<Cell>());
            previousStateCells.Add(new List<CellItem>());
            for(int col = 0; col <= length; col++) {
                if (PositionInGrid(row, col)) {
                    cells[row].Add(new Cell(row, col));
                } else {
                    cells[row].Add(null);
                }

                previousStateCells[row].Add(CellItem.None);
            }
        }
    }

    // State Management

    public void SaveCurrentState() {
        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                Cell cell = cells[row][col];

                if (cell != null) {
                    previousStateCells[row][col] = cell.currentItem;
                }
                
            }
        }

        previousStateCurrentSelectionPosition = currentSelectionPosition;
    }

    public void LoadPreviousState() {
        UnselectCell();

        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                CellItem previousCellItem = previousStateCells[row][col];
                Cell currentCell = cells[row][col];

                if (currentCell != null) {
                    currentCell.ResetItem();

                    currentCell.SetCellItem(previousCellItem);
                    currentCell.DisplayCellItem();
                }
            }
        }

        currentSelectionPosition = previousStateCurrentSelectionPosition;
        selectedPosition = null;
        UpdateSelectionObject();
    }

    public void CreateCellItem(CellItem cellItem) {
        Cell cell = GetRandomEmptyCell();

        cell.ResetItem();
        cell.SetCellItem(cellItem);
        cell.DisplayCellItem();
    }

    // Selection

    public void Select() {
        Cell targetCell = GetCell(currentSelectionPosition);

        if (targetCell.currentItem != CellItem.None) {
            // Selecting item, doesn't matter current state, this is the item that should be selected
            Narrator.Instance.selected = true;
            UnselectCell();
            SelectCell();
        } else {
            // Targeting empty cell

            // We have a selected cell
            if (selectedPosition != null) {
                Narrator.Instance.movedCell = true;
                // Let's try and move the selected cell

                // Move if adjacent
                if (CellsAdjacent(currentSelectionPosition, selectedPosition.GetValueOrDefault())) {
                    // Move CellItem
                    Cell sourceCell = GetCell(selectedPosition.GetValueOrDefault());

                    targetCell.SetCellItem(sourceCell.currentItem);
                    targetCell.DisplayCellItem();
                    sourceCell.SetCellItem(CellItem.None);
                    sourceCell.DisplayCellItem();

                    SelectCell();

                // Unselect if invalid
                } else {
                    UnselectCell();
                }
            }
        }

        UpdateSelectionObject();
    }

    private void SelectCell() {
        UnselectCell();

        selectedPosition = currentSelectionPosition;
        Cell cell = GetCell(currentSelectionPosition);

        // Create valid displays
        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                if (CellsAdjacent(new Vector2(col, row), currentSelectionPosition) && PositionInGrid(row, col)) {
                    GameObject validGo = Instantiate(cellSelectionValidPrefab, new Vector2(col - 2.5f, row - 2.5f), Quaternion.identity) as GameObject;

                    validGo.transform.SetParent(player.transform, false);
                }
            }
        }
    }

    public void UnselectCell() {
        // Delete all valid displays
        GameObject.FindGameObjectsWithTag("CellSelectionValid").ToList().ForEach(go => Destroy(go));
        selectedPosition = null;
        itemTooltip.SetActive(false);
    }

    private void UpdateSelectionObject() {
        selectionObject.transform.localPosition = currentSelectionPosition - new Vector2(2.5f, 2.5f);

        if (selectedPosition != null) {
            selectionObject.transform.Find("Selected").gameObject.SetActive(true);
            selectionObject.transform.Find("Unselected").gameObject.SetActive(false);
        } else {
            selectionObject.transform.Find("Selected").gameObject.SetActive(false);
            selectionObject.transform.Find("Unselected").gameObject.SetActive(true);
        }
    }

    public void MoveSelection(int rowDelta, int colDelta) {
        Narrator.Instance.movedSelection = true;

        Vector2 newPosition = currentSelectionPosition + new Vector2(colDelta, rowDelta);

        if (PositionInGrid(newPosition)) {
            currentSelectionPosition = newPosition;
            UpdateSelectionObject();

            itemTooltip.transform.localPosition = newPosition - new Vector2(2.5f, 2.5f) - new Vector2(0, 0.35f);

            Cell cell = GetCell(newPosition);

            if (cell.currentItem != CellItem.None) {
                itemTooltip.SetActive(true);
                itemTooltip.GetComponentInChildren<Text>().text = cell.currentItem.ToString();
            } else {
                itemTooltip.SetActive(false);
            }
        }
    }

    // State Checking

    private bool HasCellItemAt(Vector2 position) {
        return cells[Mathf.RoundToInt(position.y)][Mathf.RoundToInt(position.x)] != null;
    }

    public Cell GetCell(Vector2 pos) {
        return cells[Mathf.RoundToInt(pos.y)][Mathf.RoundToInt(pos.x)];
    }

    public Cell GetCell(int row, int col) {
        return cells[row][col];
    }

    public static bool CellsAdjacent(Vector2 a, Vector2 b) {
        Vector2 difference = a - b;

        int rowDelta = Mathf.Abs(Mathf.RoundToInt(difference.y));
        int colDelta = Mathf.Abs(Mathf.RoundToInt(difference.x));

        return (rowDelta == 1 && colDelta == 0) || (rowDelta == 0 && colDelta == 1);
    }

    public static bool CellsAdjacent(Cell a, Cell b) {
        Vector2 difference = new Vector2(a.col, a.row) - new Vector2(b.col, b.row);

        int rowDelta = Mathf.Abs(Mathf.RoundToInt(difference.y));
        int colDelta = Mathf.Abs(Mathf.RoundToInt(difference.x));

        return (rowDelta == 1 && colDelta == 0) || (rowDelta == 0 && colDelta == 1);
    }

    // Health

    public void DamageCells() {
        int cellsToDamage = 1;

        for(int i=0; i < cellsToDamage; i++) {
            Cell cell = GetRandomCellWithHealth();

            if (cell != null) {
                cell.Damage();
            }
        }
    }

    public int CalculateHealth() {
        int health = 0;

        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                Cell cell = GetCell(row, col);

                if (cell != null) {
                    health += cell.currentHealth;
                }
            }
        }

        return health;
    }

    public Cell GetRandomCellWithHealth() {
        List<Cell> validCells = new List<Cell>();

        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                Cell cell = GetCell(row, col);

                if (cell != null && cell.currentHealth > 0) {
                    validCells.Add(cell);
                }
            }
        }

        if (validCells.Count() > 0) {
            return validCells[Random.Range(0, validCells.Count() - 1)];
        } else {
            return null;
        }
    }

    public Cell GetMostDamagedCellAround(Cell aroundCell) {
        List<Cell> validCells = new List<Cell>();

        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                Cell cell = GetCell(row, col);

                if (cell != null && (CellsAdjacent(cell, aroundCell) || aroundCell.row == row && aroundCell.col == col) && cell.currentHealth < cell.startingHealth) {
                    validCells.Add(cell);
                }
            }
        }

        return validCells.OrderBy(cell => cell.currentHealth).FirstOrDefault();
    }

    public void HealAll() {
        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                Cell cell = GetCell(row, col);

                if (cell != null) {
                    cell.FullyHeal();
                }
            }
        }
    }

    // Utils

    public Cell GetRandomEmptyCell() {
        List<Cell> validCells = new List<Cell>();

        for (int row = 0; row <= length; row++) {
            for (int col = 0; col <= length; col++) {
                Cell cell = GetCell(row, col);

                if (cell != null && cell.currentItem == CellItem.None) {
                    validCells.Add(cell);
                }
            }
        }

        if (validCells.Count() > 0) {
            return validCells[Random.Range(0, validCells.Count() - 1)];
        } else {
            return null;
        }
    }

    // Static Utils

    public static bool PositionInGrid(Vector2 pos) {
        return PositionInGrid(Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.x));
    }

    public static bool PositionInGrid(int row, int col) {
        return row >= 0 && row <= length && col >= 0 && col <= length && validPositionInGrid[row][col] == 1;
    }

    public static bool IsOnPerimeter(Cell cell) {
        return cell.row >= 0 && cell.row <= length && cell.col >= 0 && cell.col <= length && validPerimeterInGrid[cell.row][cell.col] == 1;
    }

    public static Vector2 CellPositionToPlayer(Cell cell) {
        return new Vector2(cell.col, cell.row) - new Vector2(2.5f, 2.5f);
    }
}
