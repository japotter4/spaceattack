﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class OutOfBounds : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collider) {
        Destroy(collider.gameObject);
    }
}
