﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Shield : MonoBehaviour {

    private Rigidbody2D playerShip;
    public GameObject shieldPrefab;
    private GameObject shield;
    private Cell cell;
    private bool isOnPerimeter = false;

    void Awake() {
        playerShip = GameObject.Find("Player").GetComponent<Rigidbody2D>();
    }

    void Update() {
        if (cell.currentHealth == 0 && shield != null) {
            Destroy(shield);
            shield = null;
        } else if (cell.currentHealth > 0 && shield == null && isOnPerimeter) {
            CreateShield();
        }
    }

    void OnDestroy() {
        // Destroy shield
        Destroy(shield);
    }

    public void ItemPlaced(Cell cell) {
        this.cell = cell;
        isOnPerimeter = Grid.IsOnPerimeter(cell);
        Vector2 direction = GetShieldDirection();

        transform.right = direction;

        if (isOnPerimeter) {
            CreateShield();
        }
    }
    
    private void CreateShield() {
        Vector2 direction = GetShieldDirection();

        // Create shield
        shield = Instantiate(shieldPrefab);
        shield.transform.SetParent(playerShip.transform);
        shield.transform.localPosition = Vector2.zero;
        shield.transform.right = direction;
    }

    public Vector2 GetShieldDirection() {
        return transform.position - playerShip.transform.position;
    }
}
