﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class PlayerHealthUI : MonoBehaviour {

    private LayoutElement barElement;
    private LayoutElement fillerElement;
    private PlayerHealth playerHealth;

    private int totalHealth;

    void Awake () {
        barElement = transform.FindChild("Bar").GetComponent<LayoutElement>();
        fillerElement = transform.FindChild("Filler").GetComponent<LayoutElement>();


        playerHealth = FindObjectOfType<PlayerHealth>();
        totalHealth = playerHealth.StartingHealth;
        playerHealth.OnHealthChange += HealthChange;
    }

    private void HealthChange(int health) {
        barElement.flexibleWidth = health;
        fillerElement.flexibleWidth = totalHealth - health;
    }
}
