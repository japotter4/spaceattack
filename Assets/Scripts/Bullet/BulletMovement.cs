﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class BulletMovement : MonoBehaviour {

    public float speed;

    void Start () {
        transform.GetComponent<Rigidbody2D>().velocity = transform.right.normalized * speed;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        Destroy(gameObject);
    }
}
