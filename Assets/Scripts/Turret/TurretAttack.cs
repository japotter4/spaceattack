﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class TurretAttack : MonoBehaviour {

    public GameObject bulletPrefab;
    public float reloadRate; // In seconds;
    public float fireRange;
    private GameObject playerShip;
    private Vector2 turretMiddleVector = Vector2.zero;
    public float maxAngle;
    private bool isOnPerimeter = false;
    private Cell cell;

    private float _lastShot = 0;

    void Awake() {
        playerShip = GameObject.Find("Player");
    }

    public void ItemPlaced(Cell cell) {
        this.cell = cell;
        isOnPerimeter = Grid.IsOnPerimeter(cell);
        turretMiddleVector = transform.position - playerShip.transform.position;
    }

    void Update() {

        if (isOnPerimeter && cell.currentHealth > 0) {
            // Aim at closest enemy
            Transform t = GameObject.FindObjectsOfType<EnemyMovement>()
                // Where enemy at correct angle
                .Where(em => Vector2.Angle(turretMiddleVector, em.transform.position - transform.position) < maxAngle)
                .Select(em => em.transform)
                .OrderBy(tt => Vector2.Distance(transform.position, tt.position))
                .FirstOrDefault();

            if (t) {
                transform.right = t.position - transform.position;

                if (Time.time - _lastShot > reloadRate) {
                    if (Vector2.Distance(transform.position, t.position) < fireRange) {
                        // Fire
                        GameObject bullet = GameObject.Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
                        bullet.transform.right = t.position - transform.position;

                        _lastShot = Time.time;
                    }
                }
            }
        }
    }
}
