﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Thruster : MonoBehaviour {

    private Rigidbody2D playerShip;
    private Vector2 thrustDirection;
    private Cell cell;
    public float thrustAmount;
    private bool isOnPerimeter = false;

    void Awake () {
        playerShip = GameObject.Find("Player").GetComponent<Rigidbody2D>();
    }

    void Update () {
        if (isOnPerimeter && cell.currentHealth > 0) {
            playerShip.AddForce(thrustDirection * thrustAmount);
        }
    }

    public void ItemPlaced(Cell cell) {
        this.cell = cell;
        isOnPerimeter = Grid.IsOnPerimeter(cell);
        thrustDirection = GetThrustDirection(cell);

        transform.right = -thrustDirection;
    }

    public Vector2 GetThrustDirection(Cell cell) {
        return playerShip.transform.position - transform.position;
    }
}
