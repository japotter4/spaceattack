﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class Narrator : MonoBehaviour {

    public float secondsPerChar;

    private string lastNarration;
    private List<string> currentScript;
    private int currentScriptLineIndex;
    private string name = "sir";

    private Game game;
    private Grid grid;
    private GameObject narrationBoxContainer;
    private GameObject narrationBox;
    private Text narrationText;
    private GameObject nextIdicator;
    private GameObject nameInputContainer;
    public bool awaitingNextLineInput = true;
    private Vector2 originalNarrationBoxContainerPosition;

    // Walkthrough
    public bool movedSelection = false;
    public bool selected = false;
    public bool movedCell = false;
    public bool zoomed = false;

    public static Narrator Instance;

    // '^' to insert pause
    private Dictionary<string, List<string>> scripts = new Dictionary<string, List<string>>
        {
            /*
                item: #ff4300
                input: #21ff00

                maxItems = 23;
            */

            // Start with little supplies +3
            // Introduce Turret +1
            { "", new List<string> {
            }},
            { "intro", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "${mysteryCaller}Hello!\nIs this thing on?\nHello?",
                "${mysteryCaller}Oh! There you are!\nThis is Captain Q speaking! You can call me Captain K though!\nWho may you be?",
                "${function_inputName}",
                "${captain}Hello ${name}!\nYou're the aid we called for, right?\nGreat!\nOur large mining operation can't wait for your backup supplies!\n...You have enough supplies don't you?",
                "......",
                "shaking_${captain}You don't?!?!\nOh this is bad...really bad. What are we going to do...maybe Zeek was right, there have been cannibalism cases before after all...it could be the new trend...Paguk looked pretty tasty last time I saw him actually...",
                "${captain}Wait! Listen ${name}, I know you screwed up, but you can fix this.\nI just need you to collect supplies on your way here.\nSince it looks like you have nothing, I'm sending you a weapon.\nYou won't need it though, it's just in case.",
                "immediate_You've received a <color=#ff4300>Turret</color>.\n<color=#ff4300>Turrets</color> fire from the ship's edge.\nThey target enemies in front of them, so keep them moving!",
                "${function_testInput}immediate_Use the <color=#21ff00>arrow keys</color> to move the target selection.\nUse <color=#21ff00>space</color> to select the target item.\nUse <color=#21ff00>space</color> again to <color=white>move</color> or <color=white>unselect</color>.\nUse the <color=#21ff00>mouse wheel</color> to zoom in and out.\nYou can only move a selection <color=blue>one</color> cell at a time.\nTry each action now to move on!",
                "Great! You're ready to command this vessel!",
            }},
            // Introduce Thruster +1
            // Add turret +1
            { "1_waveEnded", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "${captain}Wow! You did better than my dear old Mumzie ever could!!\nJust barely though...you sure you've piloted before?\nWell whatever...there's more enemies incoming!\nQuick! I'm beaming you a <color=#ff4300>Thruster</color>.\nUse it to kite those suckers! Or run away...",
                "immediate_You've received a <color=#ff4300>Thruster</color>.\n<color=#ff4300>Thrusters</color> only work on the ship's edge.",
                "${captain}I'm also hooking you up with another <color=#ff4300>turret</color>.\nI was feeling a little sorry for you during the last fight..."
            }},
            // Introduce Repair +1
            // Add luggage +2
            // Add supplies +2
            { "2_waveEnded", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "${captain}Looks like you barely made it out of there!\nStill, you're going pretty slow. Im starting to think you forgot about us...\nHurry up!",
                "${captain}It looks like you took some damage in that last encounter though.\nHere, let me make it up to you. I've got a spare <color=#ff4300>Repair</color> unit.\nI want you to have it. Don't worry about payment...you can pay when you get here.",
                "immediate_You've received a <color=#ff4300>Repair</color> unit.\n<color=#ff4300>Repair</color> units can be placed on a cell to repair nearby cells over time.\nIf a cell gets completely destroyed, anything you put in it won't work until it's repaired.",
                ".......",
                "${captain}Hey ${name}, I noticed you're going to be passing by the Kyxiu sector.\nYou see, I'm going to need a favor.\nI made a very important purchase on Unimazon and it would be faster if you just picked it up in Kyxiu.\nThink you can do that?",
                "${captain}Great! I'm arranging for the packages to be loaded onto your ship now.\nI also know you don't have enough supplies for us so I'm loading more as well.\nI hope you have enough room!",
            }},
            // Introduce Shield +1
            // Add chest +2
            { "3_waveEnded", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "${captain}You're looking spiffy-er than ever ${name}!\nListen, I really appreciate that you volunteered bring my packages.\nIt almost makes me feel like we're friends.\nI'd have to lower my friend standards though so let's just keep it at acquaintances.",
                "${captain}Since you're getting closer to us, you'll be seeing more of the Zellotian fleet.\nI had an idea the other day to use this piece of scrap metal I found as a <color=#ff4300>shield</color>.\nHere! You can have it!",
                "immediate_You've received a <color=#ff4300>Shield</color>.\n<color=#ff4300>Shields</color> must be placed on the ship's edge.\nThey generate a small shield on the outside of the ship that blocks enemy fire.",
                "......",
                "${captain}Pssst, ${name}...keep your voice down! Listen, you're a cool person, right?\nGood. The thing is, I need someone I can trust to bring something to me.\nNo! Stop asking questions! You said you were cool!",
                "${captain}I'm loading your ship with a couple chests. Don't worry, you won't get in touble if you don't get caught. KTHXBAI!"
            }},
            // Introduce Power +3
            // Promise more room
            // Load up on supplies +3
            { "4_waveEnded", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "${captain}It's ${name}! Our savior! Those buggers didn't stand a chance!\n......\nIs what I would say if you purchased my \"24 Hours of Sidekick !Hype\".\nHowever, seeing as we're strangers, I won't be offering my acquaintance discount.\nHurry while supplies last!",
                "${captain}With all that extra power you're packing, I noticed you're a little low on juice.\nLet me hook you up with the latest model of <color=#ff4300>power</color> pack I just received.",
                "${captain}We have so many now! It's called the Zamsung Mote 7.\nI forget why we have so many...I'm sure it's fine!",
                "immediate_You've received <color=#ff4300>Power</color> packs.\n<color=#ff4300>Power</color> packs do nothing. Your popularity will explode if you use them though.\n\nDisclaimer. All sales are final. We assume no responsibilty, ever.",
                "${captain}Good news ${name}!\nI've been talking to the big Kahunas over here and I'm about to arrange a remote upgrade of your ship!",
                "I noticed you were picking up a lot of things on your way here.\nI figure you're a hoarder and I want to support that!\nYou should be getting more room any day now!",
                "${captain}With that in mind, you'll be fine bringing us more supplies, right?"
            }},
            // Promise more room and endless fame
            { "5_waveEnded", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "${captain}We can see you on the space horizon!\nOh...that was just a speck on our window.\nOur sensors do tell us you're close though...\n...",
                "......",
                "......",
                "shaking_${captain}HURRY THE @^!% UP ${name}! WHATEVER YOU DO, DON'T LOOK UP UP, DOWN DOWN, LEFT, RIGHT, LEFT, OR RIGHT OF YOU!\nEVERYTHING IS FINE! JUST KEEP YOUR EYES ON THE PRIZE!\n\n\nGodspeed ${name}..."
            }},
            // Last one
            // Thank the player, make fun of player
            { "6_waveEnded", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "shaking_${captain}WOW! You did it, ${name}!\nI always knew you could do it. Ever since you killed that last enemy.",
                "${captain}Let's take a look at these supplies!\n...\nThese supplies...are the wrong ones! We're vegan miners!\nWow ${name}. I have to say, there goes any chance of friendship we had.\nAnd you can consider brunch cancelled.\nNow get out of here!"
            }},
            { "died", new List<string> {
                "shaking_~Bzzt!\nIncoming transmission...\nConnecting to S.S. Slackinbig...",
                "${captain}We're getting you out of there!",
                "${captain}That was close...\nYou're in luck, we've remotely repaired you!\nI'm afraid you'll have to try again...\nGood luck!"
            }}
        };

    void Awake () {
        Instance = this;
        game = FindObjectOfType<Game>();
        grid = FindObjectOfType<Grid>();
        narrationBoxContainer = GameObject.Find("NarrationBoxContainer");
        narrationBox = GameObject.Find("NarrationBox");
        narrationText = GameObject.Find("NarrationText").GetComponent<Text>();
        nextIdicator = GameObject.Find("NextIndicator");
        nameInputContainer = GameObject.Find("NameInputContainer");
        nextIdicator.SetActive(false);
        narrationBox.SetActive(false);
        nameInputContainer.SetActive(false);

        originalNarrationBoxContainerPosition = narrationBoxContainer.transform.position;
    }

    void Start() {
        grid.CreateCellItem(CellItem.Supplies);
        grid.CreateCellItem(CellItem.Supplies);
        grid.CreateCellItem(CellItem.Supplies);

        //StartAtWave(6);

        StartNarration("intro");

        //StartNarration("1_waveEnded");
        //StartNarration("");
        //grid.CreateCellItem(CellItem.Turret);
        //grid.CreateCellItem(CellItem.Turret);
        //grid.CreateCellItem(CellItem.Thruster);
        //grid.CreateCellItem(CellItem.Thruster);
        //grid.CreateCellItem(CellItem.Shield);
        //grid.CreateCellItem(CellItem.Luggage);
        //grid.CreateCellItem(CellItem.Chest);
        //game.StartWave(1);
    }

    public void StartAtWave(int wave) {
        awaitingNextLineInput = false;
        AddItemsForNarration("intro");

        for(int i=1; i < wave; i++) {
            AddItemsForNarration(i + "_waveEnded");
        }

        game.StartWave(wave);
    }

    public void Next() {
        if (awaitingNextLineInput) {
            NextLine();
        }
    }

    public void NextLine() {
        StopAllCoroutines();
        narrationBoxContainer.transform.position = originalNarrationBoxContainerPosition;

        currentScriptLineIndex++;

        if (currentScriptLineIndex < currentScript.Count()) {
            string line = currentScript[currentScriptLineIndex];
            bool dontBlockInput = false;

            Regex rgx = new Regex(@"\$\{(function_.*)\}");
            Match match = rgx.Match(line);

            if (match.Success) {

                Group group = match.Groups[1];

                string function = group.ToString().Split('_')[1];

                switch (function) {
                    case "inputName":
                        // Show name input box
                        dontBlockInput = true;
                        awaitingNextLineInput = false;
                        nameInputContainer.SetActive(true);
                        break;
                    case "testInput":
                        dontBlockInput = true;
                        StartCoroutine(TestInput());
                        break;
                }

                line = rgx.Replace(line, "");
            }

            if (line.Contains("shaking_")) {
                StartCoroutine(ShakeScreen());
                line = line.Replace("shaking_", "");
            }

            StartCoroutine(WriteLine(line, dontBlockInput));
        } else {
            EndOfScript();
        }
    }

    private IEnumerator TestInput() {
        awaitingNextLineInput = false;

        while(!movedSelection || !selected || !movedCell || !zoomed) {
            yield return new WaitForEndOfFrame();
        }

        awaitingNextLineInput = true;
        Next();
    }

    public IEnumerator ShakeScreen() {
        float shake = 0.5f;
        float decreaseAmount = 1.0f;
        float shakeAmount = 100f;
        float scale = 6;

        int xRow = Random.Range(0, 1000000);
        int yRow = Random.Range(0, 1000000);

        while (true) {
            if (shake > 0) {
                narrationBoxContainer.GetComponent<RectTransform>().anchoredPosition = 
                    new Vector2(
                        (Mathf.PerlinNoise(xRow, Time.time * scale) - 0.5f) * shakeAmount, 
                        (Mathf.PerlinNoise(yRow, Time.time * scale) - 0.5f) * shakeAmount);

                shake -= Time.deltaTime * decreaseAmount;
            } else {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        narrationBoxContainer.transform.position = originalNarrationBoxContainerPosition;
    }

    public void EnteredName() {
        name = nameInputContainer.GetComponentInChildren<InputField>().text;
        nameInputContainer.SetActive(false);
        awaitingNextLineInput = true;
        Next();
    }

    private void EndOfScript() {
        narrationBox.SetActive(false);
        currentScript = null;
        awaitingNextLineInput = false;

        AddItemsForNarration(lastNarration);

        switch (lastNarration) {
            case "intro":
                game.StartWave(1);
                break;

            case "1_waveEnded":
                game.StartWave(2);
                break;

            case "2_waveEnded":
                game.StartWave(3);
                break;

            case "3_waveEnded":
                game.StartWave(4);
                break;

            case "4_waveEnded":
                game.StartWave(5);
                break;

            case "5_waveEnded":
                game.StartWave(6);
                break;

            // Last wave ended
            case "6_waveEnded":
                // Show credits screen
                SceneManager.LoadScene("Credits");
                break;
            case "died":
                game.StartWave(game.currentWave);
                break;
        }
    }

    private void AddItemsForNarration(string narration) {
        switch (narration) {
            case "intro":
                grid.CreateCellItem(CellItem.Turret);
                break;

            case "1_waveEnded":
                grid.CreateCellItem(CellItem.Thruster);
                grid.CreateCellItem(CellItem.Turret);
                break;

            case "2_waveEnded":
                grid.CreateCellItem(CellItem.Repair);
                grid.CreateCellItem(CellItem.Repair);
                grid.CreateCellItem(CellItem.Luggage);
                grid.CreateCellItem(CellItem.Supplies);
                grid.CreateCellItem(CellItem.Supplies);
                break;

            case "3_waveEnded":
                grid.CreateCellItem(CellItem.Shield);
                grid.CreateCellItem(CellItem.Chest);
                grid.CreateCellItem(CellItem.Chest);
                break;

            case "4_waveEnded":
                grid.CreateCellItem(CellItem.Turret);
                grid.CreateCellItem(CellItem.Power);
                grid.CreateCellItem(CellItem.Power);
                grid.CreateCellItem(CellItem.Shield);
                grid.CreateCellItem(CellItem.Supplies);
                grid.CreateCellItem(CellItem.Supplies);
                break;
        }
    }

    IEnumerator WriteLine(string line, bool dontBlockInput = false) {
        nextIdicator.SetActive(false);
        line = line.Replace("${name}", name);

        if (!dontBlockInput) {
            awaitingNextLineInput = false;
        }

        bool hasCaptainName = line.Contains("${captain}");
        line = line.Replace("${captain}", "");

        bool hasMysteryCaller = line.Contains("${mysteryCaller}");
        line = line.Replace("${mysteryCaller}", "");

        Regex rgx = new Regex(@"\$\{.*\}");
        line = rgx.Replace(line, "");

        string linePrefix = "";

        if (hasCaptainName) {
            linePrefix = "<color=#00ffdd>Capt K</color>: ";
        } else if (hasMysteryCaller) {
            linePrefix = "<color=#00ffdd>???</color>: ";
        }

        if (line.Contains("immediate_")) {
            line = line.Replace("immediate_", "");
            narrationText.text = linePrefix + line;
        } else {
            int lineLength = line.Count();
            // Write each letter to to text box
            for (int i = 0; i < lineLength; i++) {

                while (line[i] == ' ') {
                    i++;
                }

                if (line[i] == '\n') {
                    yield return new WaitForSeconds(0.5f);
                    i++;
                }

                if (line[i] == '<') {
                    int numTerminatorsSeen = 0;

                    while(numTerminatorsSeen < 2) {
                        i++;

                        if (line[i] == '>') {
                            numTerminatorsSeen++;
                        }
                    }
                }

                string displayText = linePrefix + line.Substring(0, i + 1);

                narrationText.text = displayText;
                yield return new WaitForSeconds(secondsPerChar);
            }

        }

        if (!dontBlockInput) {
            nextIdicator.SetActive(true);
            awaitingNextLineInput = true;
        }

        yield return new WaitForEndOfFrame();
    }

    public void StartNarration(string script) {
        lastNarration = script;
        narrationBox.SetActive(true);
        narrationText.text = "";

        currentScript = scripts[script];
        currentScriptLineIndex = -1;

        NextLine();

        //StartCoroutine(WriteLine(currentScript[currentScriptLineIndex]));
    }
}
