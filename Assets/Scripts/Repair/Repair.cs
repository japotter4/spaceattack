﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Repair : MonoBehaviour {

    public float repairRate;
    private Rigidbody2D playerShip;
    private Cell cell;
    private float repairStart = 0;

    void Awake() {
        playerShip = GameObject.Find("Player").GetComponent<Rigidbody2D>();
    }

    void Update() {
        Cell cellToRepair = Grid.Instance.GetMostDamagedCellAround(cell);

        if (cellToRepair != null) {
            if (repairStart == 0) {
                repairStart = Time.time;
            } else if (Time.time - repairStart > repairRate) {
                cellToRepair.Heal();
                repairStart = 0;
            }
        } else {
            repairStart = 0;
        }


        //if (Time.time - lastRepair > repairRate) {
        //    // Repair

        //    if (cellToRepair != null) {
        //        cellToRepair.Heal();
        //        lastRepair = Time.time;
        //    }
        //}
    }

    public void ItemPlaced(Cell cell) {
        this.cell = cell;
    }
}
