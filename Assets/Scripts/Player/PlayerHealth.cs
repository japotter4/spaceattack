﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class PlayerHealth : MonoBehaviour {

    public int StartingHealth { get { return 24 * 3; } }
    [HideInInspector]
    public int damagePerBullet;
    private Game game;

    public event Action<int> OnHealthChange;

    void Awake () {
        game = GameObject.FindObjectOfType<Game>();
    }

    public void HealthChanged() {
        // Calculate health
        int currentHealth = Grid.Instance.CalculateHealth();

        // Send health message
        OnHealthChange(currentHealth);

        // Check if dead
        if (currentHealth <= 0) {
            OnHealthChange(currentHealth);
            game.PlayerDied();
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        // Damage cells
        Grid.Instance.DamageCells();
    }
}
