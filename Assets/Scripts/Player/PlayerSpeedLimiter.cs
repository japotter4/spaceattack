﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PlayerSpeedLimiter : MonoBehaviour {

    public float maxSpeed;
    private Rigidbody2D playerBody;

    void Awake () {
        playerBody = transform.GetComponent<Rigidbody2D>();
    }

    void FixedUpdate () {
        float magnitude = playerBody.velocity.magnitude;

        if (magnitude > maxSpeed) {
            playerBody.velocity = playerBody.velocity.normalized * maxSpeed;
        }
    }
}
