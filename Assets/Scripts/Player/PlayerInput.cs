﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PlayerInput : MonoBehaviour {

    public Grid grid;
    public Narrator narrator;

    void Start () {

    }

    void Update () {
        if (Input.GetButtonDown("moveUp")) {
            grid.MoveSelection(1, 0);
        } else if (Input.GetButtonDown("moveLeft")) {
            grid.MoveSelection(0, -1);
        } else if (Input.GetButtonDown("moveRight")) {
            grid.MoveSelection(0, 1);
        } else if (Input.GetButtonDown("moveDown")) {
            grid.MoveSelection(-1, 0);
        }

        if (Input.GetButtonDown("select")) {
            if (narrator.awaitingNextLineInput) {
                narrator.Next();
            } else {
                grid.Select();
            }
        }

        //if (Input.GetButtonDown("recordState")) {
        //    grid.SaveCurrentState();
        //} else if (Input.GetButtonDown("applyPreviousState")) {
        //    grid.LoadPreviousState();
        //}
    }
}
