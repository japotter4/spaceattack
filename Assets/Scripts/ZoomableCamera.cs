﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ZoomableCamera : MonoBehaviour {

    public float minSize = 8;
    public float maxSize = 18;
    public float zoomScale;
    private Camera camera;

    void Start () {
        camera = transform.GetComponent<Camera>();
    }

    void Update () {
        float zoomDelta = Input.GetAxis("Mouse ScrollWheel");

        if (zoomDelta != 0) {
            Narrator.Instance.zoomed = true;
        }

        float currentSize = camera.orthographicSize;

        currentSize -= zoomDelta * zoomScale;

        currentSize = Mathf.Clamp(currentSize, minSize, maxSize);

        camera.orthographicSize = currentSize;
    }
}
