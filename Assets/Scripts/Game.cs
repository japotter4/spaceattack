﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Game : MonoBehaviour {

    public static float GameFarEdge = 25f;
    public EnemySpawner enemySpawner;
    private Grid grid;
    public int currentWave = 0;
    private Narrator narrator;
    private Rigidbody2D playerShip;

    void Start () {
        narrator = FindObjectOfType<Narrator>();
        grid = FindObjectOfType<Grid>();
        playerShip = GameObject.Find("Player").GetComponent<Rigidbody2D>();
    }

    public void StartWave(int wave) {
        currentWave = wave;
        grid.SaveCurrentState();
        grid.HealAll();
        enemySpawner.SpawnWave(currentWave);
    }

    public void PlayerDied() {
        enemySpawner.DestroyAllEnemies();
        grid.LoadPreviousState();
        playerShip.velocity = Vector2.zero;
        grid.HealAll();
        narrator.StartNarration("died");
    }

    public void EnemyDied() {
        if (!enemySpawner.spawning) {
            int enemyCount = FindObjectsOfType<EnemyMovement>().Count() - 1;

            if (enemyCount == 0) {
                enemySpawner.DestroyAllEnemies();
                grid.UnselectCell();
                narrator.StartNarration(currentWave.ToString() + "_waveEnded");
            }
        }
    }
}
